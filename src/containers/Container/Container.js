import React, {Component} from "react";
import {connect} from "react-redux";
import numbers from "../../store/numbers";
import "./Container.css";
let numArray = [];
for (let i in numbers) {
    numArray.push({name: i, val: numbers[i]});
}
;

class Container extends Component {


    render() {


        return (<div className="table">
            <input type={ this.props.isAns} className={this.props.isAns} disabled value={this.props.display}/>
            {
                numArray.map((num, index) => {
                    return <button className={`calc-button ${num.name}`} onClick={this.props[num.name]}
                                   key={index}>{num.val}</button>
                })

            }
        </div>)
    }


}
;
const mapStateToProps = state => {
    return {
        display: state.display,
        isAns: state.isAns
    }
};

const mapDispatchToProps = dispatch => {

    return {

        ONE: () => dispatch({type: "ONE"}),
        TWO: () => dispatch({type: "TWO"}),
        THREE: () => dispatch({type: "THREE"}),
        FOUR: () => dispatch({type: "FOUR"}),
        FIVE: () => dispatch({type: "FIVE"}),
        SIX: () => dispatch({type: "SIX"}),
        SEVEN: () => dispatch({type: "SEVEN"}),
        EIGHT: () => dispatch({type: "EIGHT"}),
        NINE: () => dispatch({type: "NINE"}),
        ZERO: () => dispatch({type: "ZERO"}),
        ENTER: () => dispatch({type: "ENTER"}),
        BACK: () => dispatch({type: "BACK"}),


    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Container);