import numbers from "../store/numbers";
import pass from "../store/pass";
const initialState = {
    display: "",
    isAns: "password"


};
const reducer = (state = initialState, action) => {
    if(state.isAns==="text")
        return {display: "", isAns: "password"};

    if ((numbers[action.type] || numbers[action.type] === 0) && numbers[action.type] >= 0) {
        if (state.display.length >= 4)
            return state;

        return {...state, display: state.display += numbers[action.type]}

    }
    else if (action.type === "BACK") {
        return {...state, display: state.display.substr(0, state.display.length - 1)};
    }
    else if (action.type === "ENTER") {
        if (pass === state.display)
            return {...state, display: "Access Granted", isAns: "text"};
        else
            return state;
    }

    return state;

};
export default reducer;