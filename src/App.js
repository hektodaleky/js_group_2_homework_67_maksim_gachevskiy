import React, { Component } from 'react';
import Counter from "./containers/Container/Container";
import './App.css';

class App extends Component {
    render() {
        return (
            <Counter/>
        );
    }
}

export default App;
